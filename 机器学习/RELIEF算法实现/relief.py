"""
# 说明：特征选择方法一：过滤式特征选择（Relief算法）
# 思想：先用特征选择过程对初始特征进行"过滤"，然后再用过滤后的特征训练模型
# 时间：2019-1-14
# 问题：
"""

import pandas as pd
import numpy as np
import numpy.linalg as la
import random


# 异常类
class FilterError:
    pass


class Filter:
    def __init__(self, data_df, sample_rate, t, k):
        """
        #
        :param data_df: 数据框（字段为特征，行为样本）
        :param sample_rate: 抽样比例
        :param t: 统计量分量阈值
        :param k: 选取的特征的个数
        """
        self.__data = data_df
        self.__feature = data_df.columns
        self.__sample_num = int(round(len(data_df) * sample_rate))#round函数：四舍五入
        self.__t = t
        self.__k = k

    # 数据处理（将离散型数据处理成连续型数据，比如字符到数值）
    def get_data(self):
        new_data = pd.DataFrame()#建立一个空二维表
        for one in self.__feature[:-1]:#遍历循环每个特征
            col = self.__data[one]#读取全部样本中其中一个特征
            #print("col:",col)
            # 判断读取到的特征是否全为数值类，如果字符串中全为数字  ，则不作改变写进新的二维表new_data里面，否则处理成数值类型写进二维表
            if (str(list(col)[0]).split(".")[0]).isdigit() or str(list(col)[0]).isdigit()\
            or (str(list(col)[0]).split('-')[-1]).split(".")[-1].isdigit():#isdigit函数：如果是字符串包含数字返回ture,否则返回false

                new_data[one] = self.__data[one]
                # print '%s 是数值型' % one
            else:
                # print '%s 是离散型' % one
                keys = list(set(list(col)))#set函数：删除重复值
                values = list(range(len(keys)))#遍历循环len（keys)
                new = dict(zip(keys, values))#dict函数就是创建一个字典，zip函数矩阵中的元素对应打包成一个元组列表
                """ print("keys:",keys)
                print("value:",values)
                print("new:",new)
                print(" self.__data[one]:", self.__data[one])
                print("self.__data[one].map(new):",self.__data[one].map(new))"""
                new_data[one] = self.__data[one].map(new)#map函数：将new: {'青绿': 0, '浅白': 1, '乌黑': 2}在col列表里做一个映射
        new_data[self.__feature[-1]] = self.__data[self.__feature[-1]]#瓜的类别属性不做改变
        return new_data

    # 返回一个样本的猜中近邻和猜错近邻
    def get_neighbors(self, row):
        df = self.get_data()
        #print("row:",row)#row是一行一行（一个一个）样本
        row_type = row[df.columns[-1]]#矩阵最后一列：瓜的类别
        #print("row_type:",row_type)
        right_df = df[df[df.columns[-1]] == row_type].drop(columns=[df.columns[-1]])#筛选出数据集类别为好瓜的样本,删除数据集最后一列
        #将删除后的数据集储存在right_df中，原数据集df保持不变。
        #print("right_df",right_df)
        wrong_df = df[df[df.columns[-1]] != row_type].drop(columns=[df.columns[-1]])
        aim = row.drop(df.columns[-1])
        #print("aim:",np.mat(aim))
        f = lambda x: eulidSim(np.mat(x), np.mat(aim))#lambda函数：定义一个隐函数，  mat函数：转换为矩阵，方便线性代数的操作
        #print("F",f)#eulidsim函数是自己定义的一个计算距离的函数
        right_sim = right_df.apply(f, axis=1)#apply函数：就是将right_df里面的每个变量，axis=1按行的顺序计算，
        #print("right_sim",right_sim)
        #apply函数解释链接： https://www.cnblogs.com/xiaodongsuibi/p/8927688.html
        right_sim_two = right_sim.drop(right_sim.idxmin())#idxmin()函数：获取某行最小的序列号
        #之所以在同类中删除距离最小的那个数据，是因为在原数据集中，包括了本身的那个数据
        #print("right_sim_two", right_sim_two)
        # print right_sim_two
        # print right_sim.values.argmax()   # np.argmax(wrong_sim)
        wrong_sim = wrong_df.apply(f, axis=1)
        #print("right_sim_two.idxmin():",right_sim_two.idxmin())
        #print("wrong_sim.idxmin():", wrong_sim.idxmin())
        # print wrong_sim
        # print wrong_sim.values.argmax()
        # print right_sim_two.idxmin(), wrong_sim.idxmin()
        return right_sim_two.idxmin(), wrong_sim.idxmin()

    # 计算特征权重
    def get_weight(self, feature, index, NearHit, NearMiss):
        data = self.__data.drop(self.__feature[-1], axis=1)
        row = data.iloc[index]
        nearhit = data.iloc[NearHit]
        nearmiss = data.iloc[NearMiss]
        if (str(row[feature]).split(".")[0]).isdigit() or str(row[feature]).isdigit() \
                or (str(row[feature]).split('-')[-1]).split(".")[-1].isdigit():
            #连续型特征权重计算方式：
            max_feature = data[feature].max()
            min_feature = data[feature].min()
            print("min_feature:", min_feature)
            right = pow(round(abs(row[feature] - nearhit[feature]) / (max_feature - min_feature), 2), 2)
            wrong = pow(round(abs(row[feature] - nearmiss[feature]) / (max_feature - min_feature), 2), 2)
            # w = wrong - right
        else:
            # 离散型特征权重计算方式：
            right = 0 if row[feature] == nearhit[feature] else 1
            wrong = 0 if row[feature] == nearmiss[feature] else 1
            # w = wrong - right
        w = wrong - right
        # print w
        return w

    # 过滤式特征选择
    def relief(self):
        sample = self.get_data()
        m, n = np.shape(self.__data)  # m为行数，n为列数
        score = []
        sample_index = random.sample(range(0, m), self.__sample_num)#random.sample函数：从指定序列中随机获取指定长度的片断。sample函数不会修改原有序列。
        print ('采样样本索引为 %s ' % sample_index)
        num = 1
        for i in sample_index:    # 采样次数
            one_score = dict()#创建一个字典
            row = sample.iloc[i]
            NearHit, NearMiss = self.get_neighbors(row)
            print ('第 %s 次采样，样本index为 %s，其NearHit行索引为 %s ，NearMiss行索引为 %s' % (num, i, NearHit, NearMiss))
            for f in self.__feature[0:-1]:
                w = self.get_weight(f, i, NearHit, NearMiss)
                one_score[f] = w
                print( '特征 %s 的权重为 %s.' % (f, w))
            score.append(one_score)
            num += 1
        f_w = pd.DataFrame(score)#将字典的格式换成表格
        print ('采样各样本特征权重如下：')
        #print (f_w)
        print ('平均特征权重如下：')
        print (f_w.mean())
        return f_w.mean()

    # 返回最终选取的特征
    def get_final(self):
        f_w = pd.DataFrame(self.relief(), columns=['weight'])
        print("f_w:",f_w)
        final_feature_t = f_w[f_w['weight'] > self.__t]#设置阈值
        print ("final_feature_t:",final_feature_t)
        final_feature_k = f_w.sort_values('weight').head(self.__k)#sort_values函数：排序函数，https://blog.csdn.net/qq_24753293/article/details/80692679
        print ("final_feature_k:",final_feature_k)
        return final_feature_t, final_feature_k


# 几种距离求解
def eulidSim(vecA, vecB):
    return la.norm(vecA - vecB)
#计算原理是：类似两点间距离计算



def cosSim(vecA, vecB):
    """
    :param vecA: 行向量
    :param vecB: 行向量
    :return: 返回余弦相似度（范围在0-1之间）"""


    num = float(vecA * vecB.T)
    denom = la.norm(vecA) * la.norm(vecB)
    cosSim = 0.5 + 0.5 * (num / denom)
    return cosSim


def pearsSim(vecA, vecB):
    if len(vecA) < 3:
        return 1.0
    else:
        return 0.5 + 0.5 * np.corrcoef(vecA, vecB, rowvar=0)[0][1]


if __name__ == '__main__':
    data = pd.read_csv('1.csv',encoding="gbk")[['色泽', '根蒂', '敲声', '纹理', '脐部', '触感', '密度', '含糖率', '类别']]
    f = Filter(data, 1, 0.2, 8)
    f.relief()
    f.get_final()
    # f.get_final()
