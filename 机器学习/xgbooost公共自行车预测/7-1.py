#导入模块
import pandas as pd
import matplotlib.pyplot as plt
import xgboost as xgb
import numpy as np
from xgboost import plot_importance
from sklearn.impute import SimpleImputer
from sklearn.feature_selection import f_regression
from sklearn.model_selection import train_test_split
from math import sqrt
#数据清洗
def data_clean(train,test,sample):
    imputer=SimpleImputer(missing_values='NaN',strategy='mean')#空值用平均数填充
    imputer.fit(train.loc[1:,:])
    train=imputer.transform(train.loc[1:,:])
    return train,test,sample
#训练和测试
def train_d(X_train, y_train, X_test):
#训练
    model=xgb.XGBRegressor(max_depth=6,learning_rate =0.05, n_estimators=250,min_child_weight=7,
                           gamma=0.2,colsample_bytree= 1, subsample=0.7,reg_alpha=0.02)
    model.fit(X_train,y_train)
#测试
    y_pred=model.predict(X_test)
    y_pred = list(map(lambda x: x if x > 0 else 0, y_pred))
    y_pred=list(map(lambda x: round(x) ,y_pred))
    return y_pred
#定义误差函数
def rmse(y_pre,y_test):
    e=[]
    for i in range(len(y_test)):
            e.append(y_pre[i]-y_test[i])
    squaredError=[]
    for h in e:
        squaredError.append(h * h)  # target-prediction之差平方
    RMSE =sqrt(sum(squaredError) / len(squaredError)) #均方根误差RMSE
    return RMSE
#定义主函数
if __name__=='__main__':
    # 读取数据
    train = pd.read_csv("train.csv")
    test = pd.read_csv("test.csv")
    sample = pd.read_csv("sample_submit.csv")
    # 删除id
    train.drop('id', axis=1, inplace=True)
    test.drop('id', axis=1, inplace=True)
    train = train.drop_duplicates(keep='first')  # 删除重复值
    train.loc[train['weather'] == 4, 'y'] = 5
    train = train[np.abs(train['y'] - train['y'].mean()) <= (3 * train['y'].std())]
    Y = train.pop('y')
    # 划分数据集
    seed = 7
    test_size = 0.33
    X_train, X_test, y_train, y_test = train_test_split(train, Y, test_size=test_size, random_state=seed)
    y_pre=train_d(X_train, y_train, X_test)
    y_test = [i for i in y_test]
    #打印损失值
    print(rmse(y_pre,y_test))
