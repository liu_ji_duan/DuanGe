#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' 通过垃圾短信数据训练朴素贝叶斯模型，并进行留存交叉验证
'''

import re
import random
import numpy as np
import matplotlib.pyplot as plt
from bayes import  NaiveBayesClassifier

ENCODING = 'ISO-8859-1'#编码
TRAIN_PERCENTAGE = 0.9#训练数据所占的百分率

def get_doc_vector(words, vocabulary):
    ''' 根据词汇表将文档中的词条转换成文档向量
    :param words: 文档中的词条列表
    :type words: list of str
    :param vocabulary: 总的词汇列表
    :type vocabulary: list of str
    :return doc_vect: 用于贝叶斯分析的文档向量
    :type doc_vect: list of int
    '''
    doc_vect = [0]*len(vocabulary)#产生一个列表[0,0......]vocabulery里有几个词就有几个0
    #print(doc_vect)
    #print(len(vocabulary))
    for word in words:
        if word in vocabulary:
            idx = vocabulary.index(word) #index：从vocabulary 中找到word位置
            doc_vect[idx] = 1 #本来列表doc_vect一开始就是全部0，然后如果word在vocabulary中，
                              # 则找到一个word在vocabulary中的位置，就把那个位置的0替换成1.
            #print(doc_vect)
    return doc_vect#返回一个列表里面可能是[0,1,1,1,1,0,1,1,1,0,1,]

def parse_line(line):
    ''' 解析数据集中的每一行返回词条向量和短信类型.
    '''
    cls = line.split(',')[-1].strip()#Python strip() 方法用于移除字符串头尾指定的字符（默认为空格或换行符）或字符序列。
                                     #注意：该方法只能删除开头或是结尾的字符，不能删除中间部分的字符。
    #print("cls:",cls)
    content = ','.join(line.split(',')[: -1])
    #print("content:",content)
    # join()：    连接字符串数组。将字符串、元组、列表中的元素以指定的字符(分隔符)连接生成一个新的字符串
    word_vect = [word.lower() for word in re.split(r'\W+', content) if word]
    #print("word_vect:",word_vect)
    #re.split(r'\W+', content)content是一个字符串，分割content，得到每个含有word字符的一个新content列表
    return word_vect, cls

def parse_file(filename):
    ''' 解析文件中的数据
    '''
    vocabulary, word_vects, classes = [], [], []
    with open(filename, 'r', encoding=ENCODING) as f:
        for line in f:
            if line:
                word_vect, cls = parse_line(line)
                vocabulary.extend(word_vect)#extend：在vocabulary列表后面增加word_vect
                word_vects.append(word_vect)#把将word_vect写进列表word_vects
                classes.append(cls)#cls就是每行词条最后的那个ham和spam
    vocabulary = list(set(vocabulary))# vocabulary是一个词库（不重复词的）
                                      # set()函数创建一个无序不重复元素集，可进行关系测试，删除重复数据，
                                      # 还可以计算交集、差集、并集等。(s1-s2)差集
                                      # (s1&s2)#交集
                                      #(s1|s2)#并集
    return vocabulary, word_vects, classes

if '__main__' == __name__:
    clf = NaiveBayesClassifier()
    vocabulary, word_vects, classes = parse_file('english_big.txt')
    # 训练数据 & 测试数据
    ntest = int(len(classes)*(1-TRAIN_PERCENTAGE)) #测试数据的数量
    test_word_vects = []
    test_classes = []
    for i in range(ntest):
        idx = random.randint(0, len(word_vects)-1)#在(0, len(word_vects)-1)之间生成随机数
        test_word_vects.append(word_vects.pop(idx))#在word_vects中移除一个idx，将移除的那个词写入test_woed_vects中
        test_classes.append(classes.pop(idx))#同理
    train_word_vects = word_vects#此时的word_vects已经移除了测试集的那部分了
    train_classes = classes
    #下面就是往上面各个定义的函数传参数，返回我们想要的值
    train_dataset = [get_doc_vector(words, vocabulary) for words in train_word_vects]
    # 训练贝叶斯模型
    cond_probs, cls_probs = clf.train(train_dataset, train_classes)
    #计算出cond_probs, cls_probs=条件概率的似然值，两种类型的概率
    # 测试模型
    error = 0
    for test_word_vect, test_cls in zip(test_word_vects, test_classes):

        test_data = get_doc_vector(test_word_vect, vocabulary)#传入数据，转化成向量
        pred_cls = clf.classify(test_data, cond_probs, cls_probs)#传入向量，预测类型
        if test_cls != pred_cls:#预测出来的类型与实际类型做判断
            print('Predict: {} -- Actual: {}'.format(pred_cls, test_cls))
            error += 1
    print('Error Rate: {}'.format(error/len(test_classes)))
    # 绘制不同类型的概率分布曲线
    fig = plt.figure()
    ax = fig.add_subplot(111)#把画布分成一行一列，然后画在从左往右从上往下的第一个
    for cls, probs in cond_probs.items():
        ax.scatter(np.arange(0, len(probs)),
                   probs*cls_probs[cls],
                   label=cls,
                   alpha=0.2)
        ax.legend()
    plt.show()