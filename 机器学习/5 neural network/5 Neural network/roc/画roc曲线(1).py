# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc  ###计算roc和auc
from sklearn.model_selection import train_test_split
# Import some data to play with
cancer = datasets.load_breast_cancer()
X = cancer.data
y = cancer.target
##变为2分类
X, y = X[y != 2], y[y != 2]
# 添加噪音特征，使问题更难解决
random_state = np.random.RandomState(0)
n_samples, n_features = X.shape
X = np.c_[X, random_state.randn(n_samples, 200 * n_features)]
# shuffle and split training and test sets

X_train, X_test, y_train, y_test =\
    train_test_split(cancer.data,cancer.target, stratify=cancer.target, random_state=42)

# Learn to predict each class against the other
#degree次方，gamma,coefficient:r,c容错率
svm = svm.SVC(kernel='linear',degree=1,gamma="auto",C=1000)
###通过decision_function()计算得到的y_score的值，用在roc_curve()函数中
y_score = svm.fit(X_train, y_train).decision_function(X_test)
print("The accuracy on training set(svc):{:}".format(svm.score(X_train, y_train)))
print("The accuracy on test set(svc):{:}".format(svm.score(X_test, y_test)))
# Compute ROC curve and ROC area for each class

fpr,tpr,threshold = roc_curve(y_test, y_score) ###计算真正率和假正率
roc_auc = auc(fpr,tpr) ###计算auc（roc曲线的面积）的值
print(roc_auc)#打印出roc面积
plt.figure()
lw = 5#线的粗细
plt.figure(figsize=(10,10))
#画出曲线fpr,tpr,显示颜色，在图画内显示标签，%占位符（感觉类似于format)
plt.plot(fpr, tpr, color='red',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc) ###假正率为横坐标，真正率为纵坐标做曲线
#画出直线（[0,1],[0,1])这表示画出x=y的一条直线，lw线的粗细
plt.plot([0, 1], [0, 1], color='red', lw=lw, linestyle='--')
#定义轴的大小
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()
