
#使用上下文管理
with open ("new","w") as f:
    f.write("hello world")
print(f.closed)    #文件已关闭，不能再写入

f.write("aaa")   #写入失败