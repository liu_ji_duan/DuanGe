#使用上下文管理
with open ("new","w") as f:
    f.write("hello world\r\n")
print(f.closed)    #文件已关闭，不能再写入
f=open("new","a")  #打开文件,  建议用上下文管理打开文件
f.write("rrrr\r\n")   #写入成功