class vow(object):
    def __init__(self,text):     #初始化对象属性
        self.text = text

    def __enter__(self):      #增加
        self.text="i say"+self.text
        return self
    def __exit__(self,exc_type,exc_value,traceback):
        self.text=self.text+"!"

with vow("i'm fine") as myvow:
    print(myvow.text)
print(myvow.text)