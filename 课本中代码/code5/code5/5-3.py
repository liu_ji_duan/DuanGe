#常规文件操作
f = open("ss","a")  #打开文件
print(f.closed)  #检查文件是否打开
f.write("hello world\r\n")
f.close()   #文件关闭
print(f.closed)   #打印True，此时文件已经关了
f=open("ss","r")
content = f.readlines()
print(content)