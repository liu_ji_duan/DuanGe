#if的嵌套与elif
i=2
if i >0:     #条件1。如果条件成立则执行这部分  （条件1执行了，以下条件均不执行）
    print("positive i")
    i=i+1
elif i >1:   #条件2。不执行
    print("i is 0")
    i=i*10
else:        #条件3。不执行
    print("negetive i")
    i=i+1