example_dict ={"a":1, "b":2}
print(type(example_dict))
for k in example_dict.keys():   #我们可以通过词典的keys()方法，来循环遍历每个元素的键
    print(example_dict[k])
for v in example_dict.values():  #通过values()方法，可以遍历每个元素的值。
    print(v)
for k,v in example_dict.items():  #或者用items方法，直接遍历每 个元素
    print(k,v)
print(example_dict.clear())  #我们也可以用clear()方法，清空整个词典
print(example_dict)