class bird(object):
    def chirp(self):
        print("make sound")
class chicken(bird):
    def chirp(self):
        super().chirp() #调用super语句可以出现在子类方法的第一句，也可以出现在在子类方法的任意的其他位置
        print("ji")
Bird = bird()
Bird.chirp() #打印make sound
summer = chicken()
summer.chirp() #打印make sound 和 ji