class Bird(object):
        feather  =True
        reproduction ="egg"
        def chirp(self,sound):
            print(sound)
class chicken(Bird): #新建了一个鸡类,在类定义时，括号里为Bird。这说明，鸡类是属于鸟类（Bird）的一个子类， 即Chicken继承自Bird。
        how_to_move ="walk" #增加一个how_to_move和edible的属性
        edible  =True

class swan(Bird):
         how_to_move = "swim"
         edible  =False
summer = chicken()#鸟类就是鸡类的父类。Chicken将享有Bird的 所有属性。尽管我们只声明了summer是鸡类，但它通过继承享有了父类的属性
print(summer.feather)
summer.chirp("ji")