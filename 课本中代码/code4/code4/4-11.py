a="abcd"
print(a.index("c"))
str="ab c de f"   #在字符串中加了一个空格
sub="d"
print(str.count(sub)) #返回：sub在str中出现的次数
print(str.find(sub))   # 返回：从左开始，查找sub在str中第一次出现的位置。如果str中不包含sub，返回 -1
                       #在字符串中加了一个空格，则打印出位置是4，若不加空格，则打印出位置是3
print(str.index(sub)) # 返回：从左开始，查找sub在str中第一次出现的位置。 #如果str中不包含sub，举出错误
print(str.rfind(sub)) # 返回：从右开始，查找sub在str中第一次出现的位置  #如果str中不包含sub，返回 -1
print(str.rindex(sub)) # 返回：从右开始，查找sub在str中第一次出现的位置 # 如果str中不包含sub，举出错误
print(str.isalnum()) #返回：True,如果所有的字符都是字母或数字，否则就False
print(str.isalpha()) #返回：True,如果所有的字符都是字母
print(str.isdigit()) #返回：True,如果所有的字符都是数字
print(str.istitle()) #返回：True，如果所有的词的首字母都是大写
print(str.isspace()) #返回：True,如果所有的字符都是空格
print(str.islower()) #返回：True,如果所有的字符都是小写
