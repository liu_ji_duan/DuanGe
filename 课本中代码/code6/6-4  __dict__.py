class Bird(object):
    feather = True
    def chirp(self):
        print("some sound")
class Chicken(Bird):
    fly = False
    def __init__(self, age):
        self.age = age
    def chirp(self):
        print("ji")
summer = Chicken(2)
print("===> summer")
print(summer.__dict__)
print("===> Chicken")
print(Chicken.__dict__)
print("===> Bird")
print(Bird.__dict__)
print("===> object")
print(object.__dict__)
summer.chirp()    #打印: 'ji'
autumn = Chicken(3)
autumn.feather = False
print(summer.feather)         # 打印True
print(autumn.__dict__)        # 结果: {"age": 3, "feather": False}