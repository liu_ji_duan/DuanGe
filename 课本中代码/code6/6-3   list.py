print([1,2,3] + [5,6,9])#两个list的对象相加时，会进行合并 列表的操作。结果为合并在一起的一个列表：
print("ssss".__add__("ffff"))
class SuperList(list):
    def __sub__(self, b):   #从第一个表中去除第二个表中出现的元素
        a = self[:]   #由于继承于list，self可以利用[:]的引用来表示整个列表
        b = b[:]
        while len(b) > 0:
            element_b = b.pop()   #默认移除列表b中最后一位元素，并返回该元素
            if element_b in a:
                a.remove(element_b)
                return a

print(SuperList([1,2,3]) - SuperList([1,3,2])) # 打印[1, 2]

a = [1,2,3,4,5,6]  # 由于继承于list，self可以利用[:]的引用来表示整个列表
b = [1,2,3,8,9]
while len(b) > 0:
    element_b = b.pop()  # pop() 函数用于移除列表中的一个元素（默认最后一个元素），并且返回该元素的值。
                        # 语法：list.pop(obj=list[-1])       //默认为 index=-1，删除最后一个列表值。
                        #obj -- 可选参数，要移除列表元素的对象。该方法返回从列表中移除的元素对象。

    f=[5]

    if f in a:
        a.remove(f)
print(element_b)
print(b)
print(a)

c=[1,2,3,4]
b=c.pop()
print(b)