class Bird(object):
    feather = True
class Chicken(Bird):
    fly = False
    def __init__(self, age):
        self.age = age
    def get_adult(self):
        if self.age > 1.0:
            return True
        else:

            return False
    adult = property(get_adult)   # property is built-in
summer = Chicken(2)
print(summer.adult)    # 返回True
summer.age = 0.5
print(summer.adult)   # 返回False