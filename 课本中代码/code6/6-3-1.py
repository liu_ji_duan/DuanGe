
class SuperList(list):
    def __sub__(self, b):   #从第一个表中去除第二个表中出现的元素
        a = self[:]   #由于继承于list，self可以利用[:]的引用来表示整个列表
        b = b[:]
        print("a=",a,"b=",b)
        i = 0
        while len(b) > 0:

            i=i+1
            element_b = b.pop()   #默认移除列表b中最后一位元素，并返回该元素,
            print("element_b=",element_b)
            print("b=",b)
            print("i=",i)
            if element_b in a:     #如果element_b的元素是属于a的一部分，则进行下面
                a.remove(element_b)    #a移除与element_b共有的元素
                print("a=",a)

                return a

print(SuperList([1,2,3]) - SuperList([2,3,4])) # 打印[1, 2]
